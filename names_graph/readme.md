# Projet
The goal of this school project was create a connected graph of names from french birth database and french death database (INSEE).

# Commands
0. Quit.
1. Display the number of unique name in the graph.
2. Display the number of related components in the graph.
3. Display the related components.
4. Display list of names in the same related component from a given name.
5. Display the forgotten names.
6. Display the associated names for a given name, in popularity order.
7. Display all the graph.
8. Display the weight between two names.
9. WIP Display most popular names in a related component.
