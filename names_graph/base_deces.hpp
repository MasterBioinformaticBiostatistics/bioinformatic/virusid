/*
	Projet de TP X2BI040
	2019/2020
	Simon CHEVOLLEAU
 */
/*
	Gestion de la base de personnes décédées pour en extraire les prénoms.
 */
#ifndef __base_deces_hpp__
#define __base_deces_hpp__
#include "base_prenoms.hpp"
#include <iostream>
#include <sstream>
#include <vector>
#include <unordered_set>
#include <map>
#include <tuple>
/*
	Fonctions de lecture d'une INSEE de prénoms au format CSV
	(https://www.insee.fr/fr/statistiques/2540004#documentation):

	prénom,sexe,ddn,nombre


	Auteur: Frédéric Goualard, Université de Nantes, 2020
 */

//VARIABLES
//pour l'ouverture du fichier csv
using name_file_t2 =  io::CSVReader<2, io::trim_chars<' '>,io::double_quote_escape<',','\"'> >;
//unordered_map d'un prénom_t et du nombre d'occurence entre le prénom_t et un autre prénom_t de base_deces_t
using arcs_t = std::unordered_map<prenom_t, int, hash_pair>;
//unordered_map d'un prénom_t ayant un lien avec d'autres prénom_t + poids de ce lien
using base_deces_t = std::unordered_map<prenom_t, arcs_t,hash_pair>;
//ensemble de prénom_t unique
using leSet_t = std::unordered_set<prenom_t, hash_pair>;
//unordered_map de prénom_t avec une valeur booléenne associée
using listeAdj_t = std::unordered_map<prenom_t, bool, hash_pair>;
//Unordered_map d'ensemble de prénom_t
//Le temps d'accès pour une valeur est meilleur par l'utilisation d'un unordered_map qu'un vector
//using liste_t =  std::unordered_map<int,<leSet_t>, hash_pair>;
using liste_t = std::map<int, leSet_t>;
//map non hashé pour afficher dans l'ordre décroissant la popularité des prénoms en lien avec un prénom donné
using popu_t = std::map<int, leSet_t, std::greater<int>>;

//FUNCTIONS
//Creation de prénom
prenom_t creationPrenom(std::string & prenom, char sexe);
//Permet de lire le fichier csv et de remplir un graphe pondéré de prénoms entre eux
void lire_base_deces(const std::string& nomfic, base_deces_t& bd);
//Affichage de tout le graphe
void afficherTout(base_deces_t& bd);
//Obtention de la composante connexe où un prénom_t se trouve
void connexeSeule(base_deces_t& bd, prenom_t prenom, leSet_t& tableauDePrenom);
//Comparaison des prénoms oubliés entre les deux bases décès et prénom
void prenomsOublie(base_deces_t& bd, base_prenoms_t& bp);
//Affichage de toutes les composantes connexes
void afficherConnexe(liste_t liste);
//Obtention de l'ensemble de prénom_t d'une composante connexe
leSet_t prenomComposante(liste_t liste, prenom_t prenom);
//Affichage des prénoms uniques, n'ayant aucun lien avec d'autres prenom_t
void prenomUnique(liste_t liste);
//Affichage des prenom_t en lien avec un prenom_t donné dans l'ordre décroissant du poids de leur lien.
std::pair<int, leSet_t> popularite(base_deces_t& bd, prenom_t& prenom);
//Affichage du poids entre deux prenom_t donné
void poidsPrenom(base_deces_t bd, prenom_t prenom1, prenom_t prenom2);
//Affichage du nombre de composante connexe
liste_t retourConnexe(base_deces_t& bd);
//Affichage des prénoms les plus populaires dans une composante connexe
void meilleurePopularite(base_deces_t& bd,liste_t& liste);


#endif // __base_deces_hpp__

