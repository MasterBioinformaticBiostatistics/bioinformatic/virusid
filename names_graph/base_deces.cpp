/*
	Projet de TP X2BI040
	2019/2020
	Simon CHEVOLLEAU
 */
/*
	Gestion de la base de personnes décédées.
 */


/*
	Fonctions de lecture d'une INSEE de prénoms au format CSV:
	prénom,sexe,ddn,nombre

	Auteur: Frédéric Goualard, Université de Nantes, 2020
 */
#include "base_deces.hpp"

//Creation d'un prenom_t à partir d'un prénom et d'un sexe
prenom_t creationPrenom(std::string & prenom, char sexe)
{
if (sexe == 'F')
	{
		prenom_t prenomCompose = prenom_t{prenom, sexe_t(2)};
		return prenomCompose;
	}else if (sexe == 'H')
	{
		prenom_t prenomCompose = prenom_t{prenom, sexe_t(1)};
		return prenomCompose;
	}else
	{
		std::cout << "Mauvais sexe entré" << std::endl;
	}
};

//Division des prénoms en un tableau de prenom_t(prenom; sexe)
const std::vector<prenom_t> splitString(const std::string& prenomStr, int sexe) {
    std::vector<prenom_t> vectorPrenom;
    std::string prenomUnique;
	//Passage de la chaîne de caractères en flux
    std::stringstream iss(prenomStr);
    while(iss >> prenomUnique)
    {
		//Ajout des diffèrent prénom et du sexe dans un prénom_t
    	prenom_t prenomSexe = prenom_t{prenomUnique,sexe_t(sexe)};
    	vectorPrenom.push_back(prenomSexe);
    }
    return vectorPrenom;
}

//Lit un fichier csv et rempli une base decès de prénoms
void lire_base_deces(const std::string& nomfic, base_deces_t& bd)
{
	try {
		name_file_t2 f(nomfic);
		//Ouverture du fichier
		f.read_header(io::ignore_extra_column,"prenom","sexe");
		std::string prenomStr;
		uint8_t sexe{};
		//Tant qu'on lit dans le fichier nos colones
		while (f.read_row(prenomStr, sexe)) {
            // Séparation des prénoms
			std::vector<prenom_t> vectorPrenom = splitString(prenomStr,sexe);
            for (auto& prenomSexe:vectorPrenom)
			{
				//Pointeur vers l'élément prenomSexe de la base de données
				const auto& iter = bd.find(prenomSexe);
				//Si vrai alors prénom_t déjà mis dans la base de données
				if (iter != bd.end())
				{
                    //On évite de créer un arcs_t d'un prénom vers lui mêmùe
					for(auto& prenomSexe2:vectorPrenom){
						if (prenomSexe != prenomSexe2)
						{
							if (bd[prenomSexe].find(prenomSexe2) == bd[prenomSexe].end())
							{
								//Initialisation à 1 du lien entre 2 prénoms
								bd[prenomSexe][prenomSexe2] = 1;
							}
							else
							{	
								//Si prenom2 déjà associé à un prénom on incrémente le lien entre les prénoms
								bd[prenomSexe][prenomSexe2]++;
							}
						}
					}
				}
				else
				{
					//Si faux initialisation d'un arc d'un prénom vers les autres prénoms d'une personne
					bd[prenomSexe] = arcs_t{};
					for(auto& prenomSexe2:vectorPrenom)
					{
						if (prenomSexe != prenomSexe2)
						{
							bd[prenomSexe][prenomSexe2]=1;
						}
					}
				}
			}
		}
	} catch (const io::error::can_not_open_file& e) {
		std::cerr << e.what() << std::endl;
		exit(1);
	}
}

// Affichage de toute la base de données des décès
void afficherTout(base_deces_t& bd)
{
	//Boucle sur chaque pair de la base de données décès
    for(auto& element:bd)
    {
		//Affichage d'un prénom
        std::cout << element.first << " : ";
		//Affichage de toutes les prénoms liés
        for (auto& element2 : element.second)
        {
            std::cout << element2.first << "(" << element2.second << ") ; ";
        }
        std:: cout << "\n" << std::endl;
    }
}

//Permet d'identifier dans un ensemble une composante connexe
void connexeSeule(base_deces_t& bd, prenom_t prenom, listeAdj_t& listeAdj, bool& finRecursion, leSet_t& tableauDePrenom)
{
    if (listeAdj[prenom]==false)
    {
		//Le prénom est passé à true s'il est vu pour la première fois pour éviter de refaire plusieurs prénoms
        listeAdj[prenom] = true;
		//Ajouter du prénom dans un ensemble
        tableauDePrenom.insert(prenom);
        for (auto& element:bd[prenom])
        {
			//Récursivité et boucle sur tous les prénoms en lien d'un prénoms
            connexeSeule(bd, element.first, listeAdj, finRecursion, tableauDePrenom);
        }
		//Permet de définir si tous les prénoms d'une composante connexe on bien était ajouté par la suite
        finRecursion = true;
    }
    else
    {
        finRecursion = false;
    }
}

//Affichage du nombre de composante connexe
//Retourne la liste des composantes connexes
liste_t retourConnexe(base_deces_t& bd)
{
    int nombre{0};
	//Permet de savoir si une composante connexe est entière
    bool Recursion = false;
	//Vecteur stockant les ensembles de composantes connexes
    liste_t liste;
	//Table permettant de savoir si un prénom est déjà passé
	listeAdj_t listeAdj;
	//Composante connexe
    leSet_t tableauDePrenom;
	//On ajoute tous les prénoms en tant que clé avec la valeur false
    for(auto& element:bd){
        listeAdj[element.first] = false;
    }
	//Pour chaque couple clé/valeur de la base décès on parcourt
    for(auto& element:bd)
    {
		//Récursivité pour parcourir tous les liens entre les prénoms
        connexeSeule(bd, element.first, listeAdj, Recursion, tableauDePrenom);
        if (Recursion == true)
        {
			//On ramène la composante connexe quand tout est finit au niveau du parcourt de prénoms ayant un lien
			liste.insert({nombre, tableauDePrenom});
			//On incrémente le compteur
			nombre++;
			//Il ne faut pas oublier de vider le tableau de prénom pour garantir l'unicité des prénoms dans les composantes connexes
			tableauDePrenom.clear();
        }
    }
	return liste;
}

//Affichage de toutes les composantes connexes
void afficherConnexe(liste_t liste)
{
	for (auto& element:liste)
	{
		for (auto& element2:element.second)
		{
			std::cout << element2 << ":";
		}
		std::cout << std::endl;
	}	
}

// Affichage des prénoms oubliés, ceux sont les prénoms qui ne sont plus donnés
void prenomsOublie(base_deces_t& bd, base_prenoms_t& bp)
{
    for(auto& element:bd)
    {
		//Comparaison si un prénom n'est pas dans la base prénom
        const auto& iter = bp.find(element.first);
        if (iter == bp.end())
        {
            std::cout << element.first << ":" << std::endl;
        }
    }
}

//Simple affichage d'une composante connexe où un prénom désiré est présent
leSet_t prenomComposante(liste_t liste, prenom_t prenom)
{	
	leSet_t retour;
	std::cout << "Voici les prénoms liés entre eux dans une même composantes où se trouve" << prenom << std::endl;
	for (auto element: liste)
	{
	//Si le prenom est dans un des ensembles, alors on affiche les prénoms de cet ensemble
	//Puisqu'il ne peut être que dans un seul ensemble on break pour arrêter la boucle
	auto iter = element.second.find(prenom);
		if (iter != element.second.end())
		{
			for (auto element2:element.second)
			{
				std::cout << element2 << "::";
				retour.insert(element2);
			}
			std::cout << std::endl;
		break;
		}
	}
	return retour;
}

// Affichage du nombre de prénoms uniques (sans lien avec d'autres)
void prenomUnique(liste_t liste)
{
	int nombre{0};
	std::cout << "Le nombre de prénoms uniques et sans lien est : " << std::endl;
	for (auto& element:liste)
	{
		if (element.second.size() == 1)
		{
			//Il suffit alors d'incrémenter un int pour chaque composante connexe ayant qu'une seule valeur à l'intérieur
			for (auto& element2:element.second)
			{
				nombre++;
			}
		}	
	}
	std::cout << nombre << std::endl;
}


// Affichage des prénoms liés à un prénom donné selon le poids entre ces 2 prénoms
std::pair<int, leSet_t> popularite(base_deces_t& bd, prenom_t& prenom)
{
	popu_t popu;
	leSet_t tableau;
	//Parcourt des prenoms liés au prénom donné
	for (auto& element:bd[prenom])
	{
		//La clé est en faite le poids et la valeur est un unordered_set auquel on insert les prénoms dès que le poids est le même
		popu[element.second].insert(element.first);
	}
	// Affichage des éléments de popu
	for (auto& element:popu)
	{
		std::cout << element.first << "--";
		for (auto& element2:element.second)
		{
			std::cout << element2;
		}
		std::cout << std::endl;
	}
	//Pointeur sur le premier élément de la map popu
	auto iter = popu.rbegin();
	//On ajoute dans la pair
	std::pair<int, leSet_t> laPair;
	laPair.first = iter->first;
	laPair.second = iter->second;
	return laPair;
}
//Montrer les 2 prénoms ayant le plus haut poids dans une composante connexe
void meilleurePopularite(base_deces_t& bd, liste_t& liste)
{
	//On parcout toutes la liste de composantes connexe
	for(auto& element:liste){
		//On prend un prénom au hasard, le lien le plus fort sera quoi qu'il arrive
		//renvoyé par la fonction popularité
		auto iter2 = element.second.begin();
		prenom_t prenom = *iter2;
		std::pair<int, leSet_t> laPair = popularite(bd, prenom);
		std::cout << '\n' << laPair.first << "\n" << std::endl;
		for (auto& element2:laPair.second)
		{
			std::cout << element2 << "--";
		}
		std::cout << std::endl;
	}
}

//Affichage du poids entre deux prenom_t donné
void poidsPrenom(base_deces_t bd, prenom_t prenom1, prenom_t prenom2)
{
	std::cout << "Affichage du poids entre deux prenom : " << std::endl;
	const auto& iter = bd.find(prenom1);
	//Le prénom 1 existe
	if (iter != bd.end())
	{
		const auto& iter2 = bd.find(prenom2);
		//Le prénom 2 existe
		if (iter2 != bd.end())
		{
			//On vérifie qu'il existe un lien entre ces 2 prénoms
			const auto& iter3 = bd[prenom1].find(prenom2);
			if (iter3 !=bd[prenom1].end())
			{
				std::cout << "Le poids entre le prénom :" << prenom1 << " et " << prenom2 << " est de : " << bd[prenom1][prenom2] << std::endl;
			}else
			{
				std::cout << "Il n'existe aucun lien entre ces 2 prénoms" << std::endl;
			}			
		}else
		{
			std::cout << prenom2 << " n'existe pas dans la base de données décès" << std::endl;
		}
	}else
	{
		std::cout << prenom1 << " n'existe pas dans la base de données décès" << std::endl;
	}
}