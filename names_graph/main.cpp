/*
	Projet de TP X2BI040
	2019/2020
	Simon CHEVOLLEAU
 */

#include "base_deces.hpp"
#include "base_prenoms.hpp"

int main(int argc, char *argv[])
{
	//Entrées sécurisés des données pour éviter les erreurs
	auto entreeSecuriseeP = [](auto &variable, auto predicat = true) -> void {
		while (!(std::cin >> variable) || !predicat(variable))
		{
			if (std::cin.eof())
			{
				throw std::runtime_error("Le flux a été fermé !");
			}
			else if (std::cin.fail())
			{
				std::cout << "Entrée invalide. Recommence." << std::endl;
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			}
			else
			{
				std::cout << "Le prédicat n'est pas respecté, merci de relire les consignes !" << std::endl;
			}
		}
	};

	auto entreeSecurisee = [](auto &variable) -> void {
		while (!(std::cin >> variable))
		{
			if (std::cin.eof())
			{
				throw std::runtime_error("Le flux a été fermé !");
			}
			else if (std::cin.fail())
			{
				std::cout << "Entrée invalide. Recommence." << std::endl;
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			}
		}
	};

	//Affichage du menu
	auto affichageMenu = []() -> void {
		std::cout << "====================================================================================================" << std::endl
				  << "Menu: " << std::endl;
		std::cout << "0 Quitter" << std::endl;
		std::cout << "1 Affichage du nombre de prénom unique dans le graphique" << std::endl;
		std::cout << "2 Affichage du nombre de composantes connexes du graphe des prénoms" << std::endl;
		std::cout << "3 Affichage des composantes connexes du graphe des prénoms" << std::endl;
		std::cout << "4 Affichage de la liste des prénoms dans la même composante connexe qu'un prénom donné au clavier" << std::endl;
		std::cout << "5 Affichage de la liste des prénoms \"oubliés\"" << std::endl;
		std::cout << "6 Affichage de la liste des prénoms associés à un prénom donné au clavier, dans l'ordre de popularité" << std::endl;
		std::cout << "7 Affichage de tout le graphique" << std::endl;
		std::cout << "8 Affichage du nombre du poids entre deux prénoms" << std::endl;
		std::cout << "9 EN COURS Affichage des prénoms les plus populaires dans une composante connexe" << std::endl;
		std::cout << "====================================================================================================" << std::endl;
	};
	if (argc != 3)
	{
		std::cout << "Merci de saisir le bon nombre de paramètre pour la bonne execution de ce programme\n"
				  << "Premièrement une base décès puis une base prénoms" << std::endl;
	}
	std::cout << "*Chargement cours*" << std::endl;
	base_deces_t bd;
	base_prenoms_t bp;
	int choix;
	char sexe;
	std::string prenom, prenomAutre;
	prenom_t prenomt, prenomtAutre, prenomCompose;
	lire_base_deces(argv[1], bd);
	lire_base_prenoms(argv[2], bp);
	liste_t liste = retourConnexe(bd);
	std::cout << "*Chargement fini*" << std::endl;
	do
	{
		affichageMenu();
		entreeSecuriseeP(choix, [](int choix) -> bool { return choix >= 0 && choix <= 9; });
		switch (choix)
		{
		case 0:
			std::cout << "Exctinction du programme" << std::endl;
			return 0;
			break;
		case 1:
			prenomUnique(liste);
			break;
		case 2:
			std::cout << "Le nombre de composante connexe est : " << liste.size() << std::endl;
			break;
		case 3:
			afficherConnexe(liste);
			break;
		case 4:
			std::cout << "Veuillez saisir un prénom en majuscule puis le sexe (H ou F)" << std::endl;
			entreeSecurisee(prenom);
			entreeSecurisee(sexe);
			if (sexe == 'F' || sexe == 'H')
			{
				prenomt = creationPrenom(prenom, sexe);
				prenomComposante(liste, prenomt);
				break;
			}
			else
			{
				std::cout << "Sexe entré invalide" << std::endl;
			}
		case 5:
			prenomsOublie(bd, bp);
			break;
		case 6:
			std::cout << "Veuillez saisir un prénom en majuscule puis le sexe (H ou F)" << std::endl;
			entreeSecurisee(prenom);
			entreeSecurisee(sexe);
			prenomt = creationPrenom(prenom, sexe);
			popularite(bd, prenomt);
			break;
		case 7:
			afficherTout(bd);
			break;
		case 8:
			std::cout << "Veuillez saisir un premier prénom en majuscule puis le deuxième et enfin le sexe" << std::endl;
			entreeSecurisee(prenom);
			entreeSecurisee(prenomAutre);
			entreeSecurisee(sexe);
			if (sexe == 'F' || sexe == 'H')
			{
				prenomt = creationPrenom(prenom, sexe);
				prenomtAutre = creationPrenom(prenomAutre, sexe);
				poidsPrenom(bd, prenomt, prenomtAutre);
			}
			else
			{
				std::cout << "Sexe entré invalide" << std::endl;
			}
			break;
		case 9:
			meilleurePopularite(bd, liste);
			break;
		}
	} while (choix != 0);
	return EXIT_SUCCESS;
}
