# Project
The goal of this M1 project was to classify virus DNA according to their human pathogenicity of not, using a multinomial naive bayes classifier.

# Data
DNA sequence have been downloaded using a virus list and a bash script.

# References
multiToOneLine.py comes from Mohamed Hamidat https://www.researchgate.net/profile/Mohamed_Hamidat
DNAsequencing.py initialy comes from Krish Naik https://github.com/krishnaik06/DNA-Sequencing-Classifier
    but light modifications have been done
