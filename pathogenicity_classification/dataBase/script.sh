
#!/usr/bin/env bash

while read p; do
    for val in $p; do
        if [[ $val == NC_* ]]; then
            NC="$val".fasta
	fi
                if [[ $val == incomplete ]]; then
                    mv "$NC" incomplete
                fi
                if [[ $val == complete ]];then
                    COMPLETE=1
                fi
                if [[ $val == human* ]];then
                    HUMAN=1
                fi
                if [[ $HUMAN == 1 ]];then
                    if [[ $COMPLETE == 1 ]];then
                        python3 multiToOneLine.py "$NC"
			NH="$NC".txt
			mv "$NH" human
                    fi
                fi
                if [[ $COMPLETE == 1 ]]; then
                        python3 multiToOneLine.py "$NC"
			NH="$NC".txt
			mv "$NH" other
                fi
    done
    HUMAN=0
    COMPLETE=0
done <$1

cd other
for i in $(find . -name '*.txt'); do
echo -e "\t0" >> $i 
tail -n +3 $i >> $i"2"
done
for i in $(find . -name '*.txt2'); do 
cat $i >> otherAll.txt
done
cd ..

cd human
for i in $(find . -name '*.txt'); do
echo -e "\t1" >> $i 
tail -n +3 $i >> $i"2"
done
for i in $(find . -name '*.txt2'); do 
cat $i >> humanAll.txt
done
cd ..


