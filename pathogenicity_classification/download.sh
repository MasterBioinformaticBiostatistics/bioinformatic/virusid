#!/usr/bin/env bash
$1

while read p; do
    wget 'https://www.ncbi.nlm.nih.gov/search/api/download-sequence/?db=nuccore&id='"$p" -O $p.fasta
done <$1